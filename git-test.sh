# Assuming that you have installed Git Bash via Git for Windows,
# this should run without failing and make a new subdirectory in
# your present working directory named `cadmus-public-git-test`.
#
# You can run it from Git Bash by being in the same directory
# location as this file and running `./git-test.sh` without the
# backticks.

git clone https://gitlab.com/cadmus-group/cadmus-public-git-test.git

if ls cadmus-public-git-test
  then echo "It worked!"
  else echo "It failed!"
fi
